#!/bin/bash
# OpenCo Reverse Proxy Build 'n' Run Script 
# Version 1.0,1

PROJECTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    PROJECT=${PROJECTPATH##*/}
FULL_BRANCH=$(git rev-parse --abbrev-ref HEAD)
# Lowecase the branch
FULL_BRANCH=`echo $FULL_BRANCH|awk '{print tolower($0)}'`
     BRANCH=${FULL_BRANCH##*/}
        TAG=$(git describe --always --tag)

docker run -d -p 80:80 -p 443:443 \
  --name $PROJECT --restart unless-stopped \
  -v /var/run/docker.sock:/tmp/docker.sock \
  -v /var/cache/certs:/etc/nginx/certs \
  openco/$PROJECT-$BRANCH:latest
